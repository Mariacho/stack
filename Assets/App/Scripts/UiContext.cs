﻿using App.Scripts.Ui.Views;

namespace App.Scripts
{
    public class UiContext
    {
        private MenuView _menuView;
        private ResultView _resultView;
        private GameView _gameView;

        public UiContext(MenuView menuView, ResultView resultView, GameView gameView)
        {
            _menuView = menuView;
            _resultView = resultView;
            _gameView = gameView;
        }

        public MenuView MenuView => _menuView;
        public ResultView ResultView => _resultView;
        public GameView GameView => _gameView;
    }
}