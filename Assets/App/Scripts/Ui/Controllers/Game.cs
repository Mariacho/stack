﻿using System;
using App.Scripts.Controllers;
using App.Scripts.Game.Common;
using App.Scripts.Ui.Views;

namespace App.Scripts.Ui.Controllers
{
    public class Game : IController
    {
        public event Action ClickedGameZone;

        private GameView _gameView;
        private ResultView _resultView;

        private ScoreCounter _scoreCounter;

        public Game(UiContext uiContext, ScoreCounter scoreCounter)
        {
            _gameView = uiContext.GameView;
            _resultView = uiContext.ResultView;
            _scoreCounter = scoreCounter;
        }
        
        public void Activate()
        {
            _gameView.Show();
            _gameView.Click += OnClickedGameZone;
            _scoreCounter.ChangeScore += ShowScore;
        }
        
        public void Deactivate()
        {
            _gameView.Hide();
            _gameView.Click -= OnClickedGameZone;
            _scoreCounter.ChangeScore -= ShowScore;
        }

        public void ShowResult()
        {
            _gameView.Hide();
            _resultView.Show();
        }
        
        private void ShowScore(int score)
        {
            _gameView.ShowScore(score);
        }
        
        private void OnClickedGameZone()
        {
            ClickedGameZone?.Invoke();
        }
    }
}