﻿using System;
using App.Scripts.Controllers;
using App.Scripts.Game;
using App.Scripts.Game.Common;
using App.Scripts.Ui.Views;

namespace App.Scripts.Ui.Controllers
{
    public class Result : IController
    {
        public event Action PlayAgain;
        
        private ResultView _resultView;
        private GameController _gameController;
        private ScoreCounter _scoreCounter;

        public Result(UiContext uiContext, GameController gameController, ScoreCounter scoreCounter)
        {
            _gameController = gameController;
            _scoreCounter = scoreCounter;
            _resultView = uiContext.ResultView;
        }

        public void Activate()
        {
            _resultView.ClickedPlayAgain += OnPlayAgain;
            _gameController.EndGame += OnEndGame;
            _scoreCounter.NewRecord += OnNewRecord;
        }
        
        public void Deactivate()
        {
            _resultView.Hide();
            _resultView.ClickedPlayAgain -= OnPlayAgain;
            _gameController.EndGame -= OnEndGame;
            _scoreCounter.NewRecord -= OnNewRecord;
        }
        
        private void OnPlayAgain()
        {
            _gameController.Deactivate();
            _resultView.Hide();
            PlayAgain?.Invoke();
        }
        
        private void OnNewRecord(int score)
        {
            ShowScore(true);
        }

        private void OnEndGame()
        {
            ShowScore(false);
        }
        
        private void ShowScore(bool record)
        {
            _resultView.ShowScore(_scoreCounter.Current, _scoreCounter.Best, record);
        }
    }
}