﻿using System;
using App.Scripts.Controllers;
using App.Scripts.Game;
using App.Scripts.Game.Common;
using App.Scripts.Ui.Views;
using UnityEngine.SocialPlatforms.Impl;

namespace App.Scripts.Ui.Controllers
{
    public class Menu : IController
    {
        private MenuView _menuView;
        private GameView _gameView;
        private ScoreCounter _scoreCounter;
        
        public event Action StartGame;
        public event Action HowToPlay;
        
        public Menu(UiContext uiContext, ScoreCounter scoreCounter)
        {
            _menuView = uiContext.MenuView;
            _gameView = uiContext.GameView;
            _scoreCounter = scoreCounter;
        }

        public void Activate()
        {
            _menuView.Show();
            UpdateBestResult();
            _menuView.ClickedHowToPlay += OnClickedHowToPlay;
            _menuView.ClickedStartGame += OnClickedStartGame;
        }
        
        public void Deactivate()
        {
            _menuView.Hide();
            _menuView.ClickedHowToPlay -= OnClickedHowToPlay;
            _menuView.ClickedStartGame -= OnClickedStartGame;
        }
        
        private void OnClickedHowToPlay()
        {
            //todo: добавить экран "как играть?"
            HowToPlay?.Invoke();
        }
        
        private void OnClickedStartGame()
        {
            Deactivate();
            StartGame?.Invoke();
        }
        
        private void UpdateBestResult()
        {
            _menuView.DrawBestResult(_scoreCounter.Best);
        }
    }
}