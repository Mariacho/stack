﻿using UnityEngine;

namespace App.Scripts.Ui
{
    public class SimpleView : MonoBehaviour
    {
        [SerializeField] private GameObject _root;

        public void SetActive(bool active)
        {
            _root.SetActive(active);
        }
    }
}