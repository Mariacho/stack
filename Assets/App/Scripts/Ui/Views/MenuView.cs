﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace App.Scripts.Ui.Views
{
    public class MenuView : DialogView, IPointerClickHandler
    {
        [SerializeField] private TMP_Text _scoreText;
        [SerializeField] private Button _howToPlayButton;
        [SerializeField] private Image _startGameZone;

        public event Action ClickedHowToPlay;
        public event Action ClickedStartGame;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            ClickedStartGame?.Invoke();
        }
        
        private void OnHowToPlayClick()
        {
            ClickedHowToPlay?.Invoke();
        }

        public void DrawBestResult(int result)
        {
            _scoreText.text = result.ToString();
        }

        protected override void Init()
        {
            _howToPlayButton.onClick.AddListener(OnHowToPlayClick);
        }
        
        protected override void Dispose()
        {
            _howToPlayButton.onClick.RemoveListener(OnHowToPlayClick);
        }
    }
}
