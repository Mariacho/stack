﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace App.Scripts.Ui.Views
{
    public class ResultView : DialogView, IPointerClickHandler
    {
        [SerializeField] private TMP_Text _youScore;
        [SerializeField] private TMP_Text _scoreInfo;

        [SerializeField] private Image _playAgainZone;

        public event Action ClickedPlayAgain;

        public void ShowScore(int score, int best, bool record)
        {
            _youScore.text = score.ToString();
            if (record)
            {
                _scoreInfo.text = "NEW RECORD";
            }
            else
            {
                _scoreInfo.text = "BEST " + best.ToString();
            }

            Show();
        }
        
        public void OnPointerClick(PointerEventData eventData)
        {
            ClickedPlayAgain?.Invoke();
        }

        protected override void Init()
        {
        }

        protected override void Dispose()
        {
        }
    }
}