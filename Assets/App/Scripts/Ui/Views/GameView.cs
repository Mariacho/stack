﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace App.Scripts.Ui.Views
{
    public class GameView : DialogView, IPointerDownHandler
    {
        [SerializeField] private TMP_Text _score;

        public event Action Click;
        
        public void ShowScore(int newScore)
        {
            _score.text = newScore.ToString();
        }

        public override void Hide()
        {
            base.Hide();
            _score.text = "0";
        }

        protected override void Init()
        {
        }

        protected override void Dispose()
        {
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Click?.Invoke();
        }
    }
}