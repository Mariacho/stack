﻿namespace App.Scripts.Ui
{
    public abstract class DialogView : SimpleView
    {
        public virtual void Show()
        {
            SetActive(true);
            Init();
        }

        public virtual void Hide()
        {
            Dispose();
            SetActive(false);
        }

        protected abstract void Init();
        protected abstract void Dispose();
    }
}