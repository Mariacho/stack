﻿using App.Scripts.Controllers;
using App.Scripts.Game;
using App.Scripts.Game.PlatformMechanics;
using UnityEngine;

namespace App.Scripts.Camera
{
    public class CameraFollow : IController, IGameState
    {
        private UnityEngine.Camera _camera;
        private Distancer _distancer;
        private GameTick _gameTick;
        
        private float _speedFollow;
        private float _rangeUp;

        private Vector3 _targetPosition;
        private Vector3 _startPosition;

        public CameraFollow(UnityEngine.Camera camera, Distancer distancer, GameTick gameTick, float speedFollow, float gangeUp)
        {
            _camera = camera;
            _distancer = distancer;
            _gameTick = gameTick;
            _speedFollow = speedFollow;
            _rangeUp = gangeUp;

            _startPosition = _camera.transform.position;
            
            Reset();
        }
        
        public void Activate()
        {
            _camera.transform.position = _startPosition;
            _distancer.AnySuccessClick += OnCorrectTargetPosition;
            _gameTick.Tick += OnCameraFollow;
        }
        
        public void Deactivate()
        {
            _distancer.AnySuccessClick -= OnCorrectTargetPosition;
            _gameTick.Tick -= OnCameraFollow;
        }
        
        private void OnCorrectTargetPosition()
        {
            _targetPosition = new Vector3(_targetPosition.x, _targetPosition.y + _rangeUp, _targetPosition.z);
        }

        private void OnCameraFollow()
        {
            _camera.transform.position = Vector3.MoveTowards(_camera.transform.position, _targetPosition,
                Time.deltaTime * _speedFollow);
        }

        public void Reset()
        {
            _targetPosition = _startPosition;
            _camera.transform.position = _startPosition;
        }
    }
}