﻿using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.Game
{
    public class Platform : MonoBehaviour
    {
        [SerializeField] private Renderer _renderer;
        private MoveDirection _moveDirection;
        private MoveDirectionAxis _moveDirectionAxis;
        public MoveDirection MoveDirection => _moveDirection;
        public MoveDirectionAxis MoveDirectionAxis => _moveDirectionAxis;

        public void Init(MoveDirection moveDirection, MoveDirectionAxis moveDirectionAxis)
        {
            _moveDirection = moveDirection;
            _moveDirectionAxis = moveDirectionAxis;
        }

        public void SetColor(Color newColor)
        {
            _renderer.material.color = newColor;
        }
    }
}