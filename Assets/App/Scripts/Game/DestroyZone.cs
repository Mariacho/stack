﻿using System;
using UnityEngine;

namespace App.Scripts.Game.PlatformMechanics
{
    public class DestroyZone : MonoBehaviour
    {
        [SerializeField] private MeshCollider _collider;

        private void OnCollisionEnter(Collision other)
        {
            Destroy(other.gameObject);
        }
    }
}