﻿using System;
using System.Runtime.InteropServices;
using App.Scripts.Camera;
using App.Scripts.Controllers;
using App.Scripts.Game.Common;
using App.Scripts.Game.PlatformMechanics;
using App.Scripts.Ui.Views;
using UnityEngine;

namespace App.Scripts.Game
{
    public class GameController : IController
    {
        public event Action EndGame;

        private Spawner _spawnerPlatform;
        private TowerDestroyer _towerDestroyer;
        private Ui.Controllers.Game _game;
        private Mover _moverPlatform;
        private Cutter _cutter;
        private GameTick _gameTick;
        private Distancer _distancer;

        private ScoreCounter _scoreCounter;
        private PlatformCounter _platformCounter;
        private CameraFollow _cameraFollow;
        
        private Platform _lastPlatform;
        private Platform _currentPlatform;

        public GameController(UiContext uiContext, GameTick gameTick, Spawner spawnerPlatform, Mover moverPlatform, Cutter cutter, Distancer distancer, ScoreCounter scoreCounter, Ui.Controllers.Game game, CameraFollow cameraFollow, TowerDestroyer towerDestroyer)
        {
            _gameTick = gameTick;
            _spawnerPlatform = spawnerPlatform;
            _towerDestroyer = towerDestroyer;
            _moverPlatform = moverPlatform;
            _cutter = cutter;
            
            _distancer = distancer;
            _game = game;

            _scoreCounter = scoreCounter;
            _cameraFollow = cameraFollow;
            
            _platformCounter = new PlatformCounter(_spawnerPlatform, _towerDestroyer);
        }

        public void StartNewGame()
        {
            _lastPlatform = _spawnerPlatform.FirstPlatform;
            _spawnerPlatform.SpawnPlatform();
        }

        public void Activate()
        {
            _gameTick.Tick += UpdateGame;
            _game.ClickedGameZone += OnPlayerClick;
            
            _distancer.SimpleClick += OnSimplePlatform;
            _distancer.FailClick += OnEndGame;
            _distancer.PerfectClick += OnPerfectClicked;
            
            _spawnerPlatform.Spawn += OnSpawned;
            _game.Activate();
            
            _scoreCounter.Activate();
            _platformCounter.Activate();
            _cameraFollow.Activate();
            
            _gameTick.Activate();
        }
        
        public void Deactivate()
        {
            _gameTick.Tick -= UpdateGame;
            _game.ClickedGameZone -= OnPlayerClick;
            
            _distancer.SimpleClick -= OnSimplePlatform;
            _distancer.FailClick -= OnEndGame;
            _distancer.PerfectClick -= OnPerfectClicked;
            
            _spawnerPlatform.Spawn -= OnSpawned;
            _game.Deactivate();
            
            _scoreCounter.Deactivate();
            _platformCounter.Deactivate();
            _cameraFollow.Deactivate();
            
            _gameTick.Deactivate();
        }

        public void ResetGame()
        {
            _platformCounter.ClearPlatforms();
            _spawnerPlatform.Reset();
            _cutter.Reset();
            _cameraFollow.Reset();
            _scoreCounter.Reset();
        }
        
        private void OnSimplePlatform(Platform sender, float distance, float cutdirecton)
        {
            _cutter.CutPlatform(sender, distance, cutdirecton);
            SpawnNextPlatform();
        }

        private void OnEndGame()
        {
            _currentPlatform.gameObject.AddComponent<Rigidbody>();
            EndGame?.Invoke();
            _scoreCounter.CheckBestScore();
            Deactivate();
            //todo: показать экран с результатом
        }

        private void OnPerfectClicked()
        {
            SpawnNextPlatform();
        }
        
        private void SpawnNextPlatform()
        {
            _lastPlatform = _currentPlatform;
            _spawnerPlatform.SpawnPlatform();
        }

        private void UpdateGame()
        {
            _moverPlatform.UpdatePosition();
        }

        private void OnPlayerClick()
        {
            _distancer.CheckDistance(_currentPlatform, _lastPlatform);
        }

        private void OnSpawned(Platform sender)
        {
            _currentPlatform = sender;
            _moverPlatform.SetPlatform(sender);
        }
        
    }
}