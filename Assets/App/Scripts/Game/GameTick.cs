﻿using System;
using App.Scripts.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.Game
{
    public class GameTick : MonoBehaviour, IController
    {
        public event Action Tick;

        private bool _isPlaying;
        
        private void FixedUpdate()
        {
            if (_isPlaying)
                Tick?.Invoke();
        }

        public void Activate()
        {
            _isPlaying = true;
        }

        public void Deactivate()
        {
            _isPlaying = false;
        }
    }
}