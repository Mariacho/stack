﻿using System;
using App.Scripts.Controllers;
using App.Scripts.Game.PlatformMechanics;
using UnityEngine;

namespace App.Scripts.Game.Common
{
    public delegate void ScoreHandler(int score);
    
    public class ScoreCounter : IController, IGameState
    {
        public event ScoreHandler ChangeScore;
        public event ScoreHandler NewRecord;
        
        public int Current => _current;
        public int Best => _best;
        
        private int _current;
        private int _best;

        private Distancer _distancer;

        public ScoreCounter(Distancer distancer)
        {
            _distancer = distancer;
            _best = PlayerPrefs.GetInt("best");
        }

        public void Activate()
        {
            _distancer.AnySuccessClick += AddScore;
        }
        
        public void Deactivate()
        {
            _distancer.AnySuccessClick -= AddScore;
        }
        
        public void CheckBestScore()
        {
            if (_current > _best)
            {
                _best = _current;
                PlayerPrefs.SetInt("best", _best);
                NewRecord?.Invoke(_best);
            }
        }

        private void AddScore()
        {
            _current++;
            ChangeScore?.Invoke(_current);
        }

        public void Reset()
        {
            _current = 0;
            ChangeScore?.Invoke(_current);
        }
    }
}