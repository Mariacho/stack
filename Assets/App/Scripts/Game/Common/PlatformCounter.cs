﻿using System.Collections.Generic;
using App.Scripts.Controllers;
using App.Scripts.Game.PlatformMechanics;
using UnityEngine;

namespace App.Scripts.Game.Common
{
    public class PlatformCounter : IController
    {
        private List<GameObject> Platforms;
        private Spawner _spawner;
        private TowerDestroyer _towerDestroyer;

        public PlatformCounter(Spawner spawner, TowerDestroyer towerDestroyer)
        {
            _spawner = spawner;
            _towerDestroyer = towerDestroyer;
            Platforms = new List<GameObject>();
        }

        public void Activate()
        {
            _spawner.Spawn += AddPlatform;
            _spawner.SpawnFallingPart += AddPlatform;
        }
        
        public void Deactivate()
        {
            _spawner.Spawn -= AddPlatform;
            _spawner.SpawnFallingPart -= AddPlatform;
        }

        public void ClearPlatforms()
        {
            _towerDestroyer.DestroyTower(Platforms);
            Platforms.Clear();
        }

        private void AddPlatform(Platform sender)
        {
            Platforms.Add(sender.gameObject);
        }
        
        private void AddPlatform(GameObject sender)
        {
            Platforms.Add(sender);
        }
    }
}