﻿using System;
using App.Scripts.Controllers;
using UnityEngine;

namespace App.Scripts.Game.PlatformMechanics
{
    public delegate void PlatformStateHandler(Platform sender, float distance, float cutDirecton);
    
    public class Distancer
    {
        private float _minDistanceSuccess;
        private float _distanceDestroy;

        private Platform _lastPlatform;
        private Platform _currentPlatform;

        public event Action AnySuccessClick;
        public event Action PerfectClick;
        public event Action FailClick;
        public event PlatformStateHandler SimpleClick;

        public Distancer(float minDistanceSuccess, float distanceDestroy)
        {
            _minDistanceSuccess = minDistanceSuccess;
            _distanceDestroy = distanceDestroy;
        }

        private bool CheckFail(float distance)
        {
            float max = _currentPlatform.MoveDirectionAxis == MoveDirectionAxis.Z ? _lastPlatform.transform.localScale.z : _lastPlatform.transform.localScale.x;
            if (Mathf.Abs(distance) >= max)
            {
                return true;
            }
            return false;
        }
        
        private float GetDistance()         
        {
            if (_currentPlatform.MoveDirectionAxis == MoveDirectionAxis.Z)
            {
                if (Mathf.Abs(_currentPlatform.transform.position.z - _lastPlatform.transform.position.z) > _minDistanceSuccess)
                    return _currentPlatform.transform.position.z - _lastPlatform.transform.position.z;
                else
                    return 0;
            } 
            else
            {
                if (Mathf.Abs(_currentPlatform.transform.position.x - _lastPlatform.transform.position.x) > _minDistanceSuccess)
                    return _currentPlatform.transform.position.x - _lastPlatform.transform.position.x;
                else
                    return 0;
            }
        }
        
        public void CheckDistance(Platform current, Platform last)
        {
            _currentPlatform = current;
            _lastPlatform = last;
            
            var distance = GetDistance();

            if (CheckFail(distance))
            {
                FailClick?.Invoke();
                return;
            }

            var direction = distance > 0 ? 1f : -1f;

            if (Mathf.Abs(distance) > _minDistanceSuccess)
            {
                SimpleClick?.Invoke(_currentPlatform, distance, direction);
                AnySuccessClick?.Invoke();
            }
            else
            {
                PerfectClick?.Invoke();
                AnySuccessClick?.Invoke();
                
                var newPos = _lastPlatform.transform.position;
                newPos.y += _lastPlatform.transform.localScale.y;
                _currentPlatform.transform.position = newPos;
            }
        }
    }
}