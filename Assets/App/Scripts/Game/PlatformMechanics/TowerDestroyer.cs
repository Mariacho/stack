﻿using System.Collections.Generic;
using UnityEngine;

namespace App.Scripts.Game.PlatformMechanics
{
    public class TowerDestroyer
    {
        public void DestroyTower(List<GameObject> platforms)
        {
            foreach (var platform in platforms)
            {
                if (platform != null)
                    Object.Destroy(platform);
            }
        }
    }
}