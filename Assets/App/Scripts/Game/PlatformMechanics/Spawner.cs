﻿using App.Scripts.Controllers;
using UnityEngine;

namespace App.Scripts.Game.PlatformMechanics
{
    public delegate void PlatformHandler(Platform sender);
    public delegate void PrimitiveHandler(GameObject sender);
    public class Spawner : MonoBehaviour, IGameState
    {
        [Range(3, 6)]
        [SerializeField] private float _distanceSpawn = 4;
        [SerializeField] private Platform _platformPrefab;
        [SerializeField] private Platform _firstPlatform;
        [SerializeField] private Transform _parentPlatform;

        private Vector3 _platformScale;
        
        public Platform FirstPlatform => _firstPlatform;

        private float _offSetY;
        private Vector3 _spawnPosX;
        private Vector3 _spawnPosZ;

        private bool _isXPosition;

        public event PlatformHandler Spawn;
        public event PrimitiveHandler SpawnFallingPart;
        
        public float DistanceSpawn => _distanceSpawn;

        private void Awake()
        {
            Reset();
        }

        public void SpawnPlatform()
        {
            InstantiatePlatform(_isXPosition);
        }

        public void SpawnFallingPlatform(float fallingPos, float fallingSize, Platform platform)
        {
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

            if (platform.MoveDirectionAxis == MoveDirectionAxis.Z)
            {
                cube.transform.localScale = new Vector3(platform.transform.localScale.x, platform.transform.localScale.y, fallingSize);
                cube.transform.position = new Vector3(platform.transform.position.x, platform.transform.position.y, fallingPos);
            }
            else
            {
                cube.transform.localScale = new Vector3(fallingSize, platform.transform.localScale.y, platform.transform.localScale.z);
                cube.transform.position = new Vector3(fallingPos, platform.transform.position.y, platform.transform.position.z);
            }
            
            cube.AddComponent<Rigidbody>();
            SpawnFallingPart?.Invoke(cube);
        }

        public void UpdateInformationForCreatePlatform(Vector3 newScale, float newPos, bool isX)
        {
            _platformScale = newScale;
            if (isX)
            {
                _spawnPosZ.x = newPos;
            }
            else
            {
                _spawnPosX.z = newPos;
            }
        }

        private void InstantiatePlatform(bool posX)
        {
            var createdPlatform = Instantiate(_platformPrefab, _parentPlatform);
            createdPlatform.transform.SetParent(_parentPlatform);
            createdPlatform.transform.localScale = _platformScale;
            
            if (posX)
            {
                createdPlatform.transform.position = _spawnPosX;
                createdPlatform.Init(MoveDirection.Down, MoveDirectionAxis.X);
            }
            else
            {
                createdPlatform.transform.position = _spawnPosZ;
                createdPlatform.Init(MoveDirection.Down, MoveDirectionAxis.Z);
            }
            Spawn?.Invoke(createdPlatform);
            _isXPosition = !_isXPosition;
            
            CorrectSpawnPosition();
        }

        private void CorrectSpawnPosition()
        {
            _spawnPosX.y += _offSetY;
            _spawnPosZ.y += _offSetY;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(new Vector3(_platformPrefab.transform.position.x, _platformPrefab.transform.position.y, _platformPrefab.transform.position.z+ _distanceSpawn), _platformPrefab.transform.localScale);
            Gizmos.DrawWireCube(new Vector3(_platformPrefab.transform.position.x-_distanceSpawn, _platformPrefab.transform.position.y, _platformPrefab.transform.position.z), _platformPrefab.transform.localScale);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(new Vector3(_platformPrefab.transform.position.x+_distanceSpawn, _platformPrefab.transform.position.y, _platformPrefab.transform.position.z), _platformPrefab.transform.localScale);
            Gizmos.DrawWireCube(new Vector3(_platformPrefab.transform.position.x, _platformPrefab.transform.position.y, _platformPrefab.transform.position.z- _distanceSpawn), _platformPrefab.transform.localScale);
        }

        public void Reset()
        {
            _offSetY = _platformPrefab.transform.localScale.y;
            _platformScale = _platformPrefab.transform.localScale;
            _spawnPosX = new Vector3(-_distanceSpawn,0, 0);
            _spawnPosZ = new Vector3(0, 0, _distanceSpawn);
            _isXPosition = true;
        }
    }
}