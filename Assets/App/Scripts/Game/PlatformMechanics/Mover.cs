﻿using App.Scripts.Controllers;
using App.Scripts.Game.PlatformMechanics;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace App.Scripts.Game.PlatformMechanics
{
    public class Mover
    {
        private Platform _platform;
        private float _maxMoveDistance;
        private float _speed;
        private MoveDirection _currentMoveDirection;

        public Mover(float speed, float maxMoveDistance)
        {
            _speed = speed;
            _maxMoveDistance = maxMoveDistance;
        }

        public void SetPlatform(Platform platform)
        {
            _platform = platform;
        }
        
        public void UpdatePosition()
        {
            if (_platform == null)
                return;

            CheckBorder(_platform.MoveDirectionAxis == MoveDirectionAxis.Z ? _platform.transform.localPosition.z : _platform.transform.localPosition.x);
            MovePlatform(_platform.MoveDirectionAxis == MoveDirectionAxis.Z ? Vector3.forward : Vector3.right);
        }

        private void CheckBorder(float position)
        {
            if (position <= -_maxMoveDistance)
                _currentMoveDirection = MoveDirection.Up;
            else if (position >= _maxMoveDistance)
                _currentMoveDirection = MoveDirection.Down;
        }

        private void MovePlatform(Vector3 direction)
        {
            if (_currentMoveDirection == MoveDirection.Up)
                _platform.transform.localPosition += direction * Time.deltaTime * _speed;
            else
                _platform.transform.localPosition -= direction * Time.deltaTime * _speed;
        }

    }
}