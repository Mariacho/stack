﻿using System;
using App.Scripts.Controllers;
using UnityEngine;

namespace App.Scripts.Game.PlatformMechanics
{
    public class Cutter : IGameState
    {
        public event Action Cut;
        
        private Platform _lastPlatform;
        private Platform _currentPlatform;
        private float _range;
        private float _platformEdge;
        private float _fallingPosition;
        private float _newPosition;
        private float _newSize;
        private float _fallingSize;
        
        private Spawner _spawner;
        
        public Cutter(Spawner spawner)
        {
            _spawner = spawner;
            Reset();
        }

        public void CutPlatform(Platform platform, float range, float cutDirection)
        {
            _currentPlatform = platform;
            _range = range;
            
            CalculateNewSize();
            CalculateNewPosition(cutDirection);
            SpawnFallingPlatform();
            Cut?.Invoke();
        }

        private void CalculateNewSize()
        {
            if (_currentPlatform.MoveDirectionAxis == MoveDirectionAxis.X)
            {
                _newSize = _lastPlatform.transform.localScale.x - Mathf.Abs(_range);
                _fallingSize = _currentPlatform.transform.localScale.x - _newSize;
            }
            else
            {
                _newSize = _lastPlatform.transform.localScale.z - Mathf.Abs(_range);
                _fallingSize = _currentPlatform.transform.localScale.z - _newSize;
            }
        }

        private void CalculateNewPosition(float direction)
        {
            if (_currentPlatform.MoveDirectionAxis == MoveDirectionAxis.X)
            {
                float newPosition = _lastPlatform.transform.position.x + (_range / 2);
                _currentPlatform.transform.localScale = new Vector3(_newSize, _currentPlatform.transform.localScale.y, _currentPlatform.transform.localScale.z);
                _currentPlatform.transform.position = new Vector3(newPosition, _currentPlatform.transform.position.y, _currentPlatform.transform.position.z);
            
                _platformEdge = _currentPlatform.transform.position.x + (_newSize / 2f * direction);
                _fallingPosition = _platformEdge + _fallingSize / 2f * direction;
            }
            else
            {
                float newPosition = _lastPlatform.transform.position.z + (_range / 2);
                _currentPlatform.transform.localScale = new Vector3(_currentPlatform.transform.localScale.x, _currentPlatform.transform.localScale.y, _newSize);
                _currentPlatform.transform.position = new Vector3(_currentPlatform.transform.position.x, _currentPlatform.transform.position.y, newPosition);
            
                _platformEdge = _currentPlatform.transform.position.z + (_newSize / 2f * direction);
                _fallingPosition = _platformEdge + _fallingSize / 2f * direction;
            }
        }
        
        private void SpawnFallingPlatform()
        {
            _lastPlatform = _currentPlatform;
            _spawner.SpawnFallingPlatform(_fallingPosition, _fallingSize, _currentPlatform);

            if (_currentPlatform.MoveDirectionAxis == MoveDirectionAxis.X)
            {
                _spawner.UpdateInformationForCreatePlatform(_currentPlatform.transform.localScale, _currentPlatform.transform.localPosition.x, true);
            }
            else
            {
                _spawner.UpdateInformationForCreatePlatform(_currentPlatform.transform.localScale, _currentPlatform.transform.localPosition.z, false);
            }
        }

        public void Reset()
        {
            _lastPlatform = _spawner.FirstPlatform;
        }
    }
}