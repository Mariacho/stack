﻿namespace App.Scripts.Controllers
{
    public interface IGameState
    {
        void Reset();
    }
}