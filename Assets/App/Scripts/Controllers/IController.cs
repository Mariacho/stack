﻿namespace App.Scripts.Controllers
{
    public interface IController
    {
        void Activate();

        void Deactivate();
    }
}