﻿using System;
using App.Scripts.Camera;
using App.Scripts.Game;
using App.Scripts.Game.Common;
using App.Scripts.Game.PlatformMechanics;
using App.Scripts.Ui.Controllers;
using App.Scripts.Ui.Views;
using UnityEngine;

namespace App.Scripts
{
    public class GamePreparer : MonoBehaviour
    {
        [Header("Camera")] 
        [SerializeField] private UnityEngine.Camera _mainCamera;
        [SerializeField] private float _speedFollow = 2;
        [SerializeField] private float _rangeUp = 0.5f;

        [Header("Platform Logic")]
        [SerializeField] private DestroyZone _destroyZone;
        [SerializeField] private Spawner _spawner;
        [SerializeField] private float _speed = 3f;
        [SerializeField] private float _minDistanceSuccess = 0.05f;
        [SerializeField] private float _distanceDestroy;

        [Header("All Ui Links")]
        [SerializeField] private MenuView _menuView;
        [SerializeField] private ResultView _resultView;
        [SerializeField] private GameView _gameView;
        
        [Header("Game Tick")] 
        [SerializeField] private GameTick _gameTick;

        private GameController _gameController;
        private Mover _mover;
        private Cutter _cutter;
        private Distancer _distancer;
        private TowerDestroyer _towerDestroyer;

        private ScoreCounter _scoreCounter;

        private UiContext _uiContext;
        private Menu _menu;
        private Ui.Controllers.Game _game;
        private Result _result;

        private CameraFollow _cameraFollow;
        
        
        private void Awake()
        {
            _uiContext = new UiContext(_menuView, _resultView, _gameView);
            _mover = new Mover(_speed, _spawner.DistanceSpawn);
            _cutter = new Cutter(_spawner);
            _distancer = new Distancer(_minDistanceSuccess, _distanceDestroy);
            _scoreCounter = new ScoreCounter(_distancer);
            _towerDestroyer = new TowerDestroyer();
            
            _cameraFollow = new CameraFollow(_mainCamera, _distancer,_gameTick, _speedFollow, _rangeUp);
            
            _game = new Ui.Controllers.Game(_uiContext, _scoreCounter);
            _gameController = new GameController(_uiContext, _gameTick, _spawner, _mover, _cutter, _distancer, _scoreCounter, _game, _cameraFollow, _towerDestroyer);

            _result = new Result(_uiContext, _gameController, _scoreCounter);
            _result.Activate();
            
            _menu = new Menu(_uiContext, _scoreCounter);
            ShowMainMenu();
        }

        private void OnEnable()
        {
            _menu.StartGame += StartGame;
            _result.PlayAgain += ShowMainMenu;
            _gameController.EndGame += _game.ShowResult;
        }

        private void OnDisable()
        {
            _menu.StartGame -= StartGame;
            _result.PlayAgain -= ShowMainMenu;
            _gameController.EndGame -= _game.ShowResult;
        }

        private void ShowMainMenu()
        {
            _gameController.ResetGame();
            _menu.Activate();
        }

        private void StartGame()
        {
            _gameController.Activate();
            _gameController.StartNewGame();
        }
 
    }
}